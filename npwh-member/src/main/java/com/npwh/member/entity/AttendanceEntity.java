package com.npwh.member.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 会员考勤记录表
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-10 10:29:37
 */
@Data
@TableName("ums_attendance")
public class AttendanceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 考勤编号
	 */
	@TableId
	private Integer id;
	/**
	 * 会员卡编号
	 */
	private Integer cardId;
	/**
	 * 会员卡号
	 */
	private String cardNumber;
	/**
	 * 会员卡类型编号
	 */
	private Integer cardTypeId;
	/**
	 * 会员卡类型名称
	 */
	private String cardTypeName;
	/**
	 * 会员资料编号
	 */
	private Integer memberId;
	/**
	 * 会员姓名
	 */
	private String memberName;
	/**
	 * 打卡时间
	 */
	private Long punchTime;
	/**
	 * 创建人
	 */
	private String createdBy;
	/**
	 * 创建时间
	 */
	private Long createdTime;
	/**
	 * 更新人
	 */
	private String updatedBy;
	/**
	 * 更新时间
	 */
	private Long updatedTime;

	/**
	 * 座位号
	 */
	@TableField(exist = false)
	private String seat;

	/**
	 * 戏剧编号
	 */
	@TableField(exist = false)
	private String dramaId;

}
