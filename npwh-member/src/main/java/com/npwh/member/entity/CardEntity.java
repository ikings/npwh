package com.npwh.member.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 会员卡表 
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@Data
@TableName("ums_card")
public class CardEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 会员卡编号
	 */
	@TableId
	private Integer id;
	/**
	 * 会员资料编号
	 */
	private Integer memberId;
	/**
	 * 会员卡号
	 */
	private String cardNumber;
	/**
	 * 会员类型编号
	 */
	private Integer cardTypeId;
	/**
	 * 会员类型名称
	 */
	private String cardTypeName;
	/**
	 * 会员类型代码
	 */
	private String cardTypeCode;
	/**
	 * 办理方式
	 */
	private String method;
	/**
	 * 办卡日期
	 */
	private Integer issueDate;
	/**
	 * 失效日期
	 */
	private Long expDate;
	/**
	 * 可用次数
	 */
	@TableField(value = "`usage`")
	private Integer usage;
	/**
	 * 可用余额
	 */
	private BigDecimal balance;
	/**
	 * 座位号
	 */
	private Integer seat;
	/**
	 * 推荐人ID
	 */
	private Integer referrerId;
	/**
	 * 推荐人名字
	 */
	private String referrer;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 乐观锁
	 */
	private Integer revision;
	/**
	 * 创建人
	 */
	private String createdBy;
	/**
	 * 创建时间
	 */
	private Long createdTime;
	/**
	 * 更新人
	 */
	private String updatedBy;
	/**
	 * 更新时间
	 */
	private Long updatedTime;

	@TableField(exist = false)
	private MemberEntity member;
	/**
	 * 操作者ID
	 */
	@TableField(exist = false)
	private Integer userId;

}
