package com.npwh.member.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 会员续费日志记录表
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-09 10:52:32
 */
@Data
@TableName("ums_card_renew")
public class CardRenewEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 续费编号
	 */
	@TableId
	private Integer id;
	/**
	 * 会员卡编号
	 */
	private Integer cardId;
	/**
	 * 金额
	 */
	private BigDecimal money;
	/**
	 * 可用次数
	 */
	@TableField(value = "`usage`")
	private Integer usage;
	/**
	 * 上次剩余可用次数
	 */
	private Integer lastusage;
	/**
	 * 当前总的可用次数
	 */
	private Integer totalusage;
	/**
	 * 有效截止日期
	 */
	private Long expiryDate;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 创建人
	 */
	private String createdBy;
	/**
	 * 创建时间
	 */
	private Long createdTime;
	/**
	 * 更新人
	 */
	private String updatedBy;
	/**
	 * 更新时间
	 */
	private Long updatedTime;

}
