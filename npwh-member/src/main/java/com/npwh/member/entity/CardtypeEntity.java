package com.npwh.member.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 会员卡类型表 
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@Data
@TableName("ums_cardtype")
public class CardtypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 会员卡类型编号
	 */
	@TableId
	private Integer id;
	/**
	 * 类型名称
	 */
	@TableField(value = "`name`")
	private String name;
	/**
	 * 卡类型编码
	 */
	@TableField(value = "`code`")
	private String code;
	/**
	 * 默认可用次数
	 */
	private Integer defusage;
	/**
	 * 消费折扣
	 */
	private String discount;
	/**
	 * 推荐人要求,满足要求均可办理
	 */
	@TableField(value = "`require`")
	private int require;
	/**
	 * 推荐办卡人会员卡类型代码 cardtype.code
	 */
	private String requireCode;
	/**
	 * 当天限制刷卡次数
	 */
	private int constrains;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 创建人
	 */
	private String createdBy;
	/**
	 * 创建时间
	 */
	private Long createdTime;
	/**
	 * 更新人
	 */
	private String updatedBy;
	/**
	 * 更新时间
	 */
	private Long updatedTime;

}
