package com.npwh.member.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 数据字典表 
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@Data
@TableName("ums_dictionary")
public class DictionaryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 字典编号
	 */
	@TableId
	private Integer id;
	/**
	 * 上级编号 ums_dictionary.id
	 */
	private Integer parentId;
	/**
	 * 类型
	 */
	private Integer type;
	/**
	 * 字典名称
	 */
	@TableField(value = "`name`")
	private String name;
	/**
	 * 描述
	 */

	@TableField(value = "`desc`")
	private String desc;
	/**
	 * 注释
	 */
	private String comments;
	/**
	 * 创建人
	 */
	private String createdBy;
	/**
	 * 创建时间
	 */
	private Integer createdTime;
	/**
	 * 更新人
	 */
	private String updatedBy;
	/**
	 * 更新时间
	 */
	private Integer updatedTime;

}
