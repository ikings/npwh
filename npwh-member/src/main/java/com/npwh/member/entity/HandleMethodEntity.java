package com.npwh.member.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 办理方式表 
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@Data
@TableName("ums_handle_method")
public class HandleMethodEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 办理方式编号
	 */
	@TableId
	private Integer id;
	/**
	 * 方式名称
	 */
	@TableField("`name`")
	private String name;
	/**
	 * 描述
	 */
	@TableField("`desc`")
	private String desc;
	/**
	 * 创建人
	 */
	private String createdBy;
	/**
	 * 创建时间
	 */
	private Integer createdTime;
	/**
	 * 更新人
	 */
	private String updatedBy;
	/**
	 * 更新时间
	 */
	private Integer updatedTime;

}
