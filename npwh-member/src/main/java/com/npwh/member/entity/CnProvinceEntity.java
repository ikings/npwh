package com.npwh.member.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 省级（省份直辖市自治区）
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 16:55:58
 */
@Data
@TableName("cn_province")
public class CnProvinceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 省份主键编号
	 */
	@TableId
	private Integer prid;
	/**
	 * 省份代码
	 */
	private String code;
	/**
	 * 省份名称
	 */
	@TableField(value = "`name`")
	private String name;
	/**
	 * 等级划分，1：一线城市；2：二线城市；3：三线城市；4：四线城市；5：五线城市
	 */
	private Integer level;

}
