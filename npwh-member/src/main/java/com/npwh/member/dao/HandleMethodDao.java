package com.npwh.member.dao;

import com.npwh.member.entity.HandleMethodEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 办理方式表 
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@Mapper
public interface HandleMethodDao extends BaseMapper<HandleMethodEntity> {
	
}
