package com.npwh.member.dao;

import com.npwh.member.entity.DramaEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-28 14:39:09
 */
@Mapper
public interface DramaDao extends BaseMapper<DramaEntity> {
	
}
