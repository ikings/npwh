package com.npwh.member.dao;

import com.npwh.member.entity.CardRenewEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员续费日志记录表
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-09 10:52:32
 */
@Mapper
public interface CardRenewDao extends BaseMapper<CardRenewEntity> {
	
}
