package com.npwh.member.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.npwh.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.npwh.member.vo.MemberVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 会员基础资料表
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
    Page<MemberVo> queryPageList(Page<MemberVo> page, @Param(Constants.WRAPPER) Wrapper<MemberVo> queryWrapper);
}
