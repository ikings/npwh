package com.npwh.member.dao;

import com.npwh.member.entity.CnCityEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 地级（城市）
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 16:55:58
 */
@Mapper
public interface CnCityDao extends BaseMapper<CnCityEntity> {
	
}
