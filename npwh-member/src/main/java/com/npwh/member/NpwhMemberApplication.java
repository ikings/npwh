package com.npwh.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NpwhMemberApplication {

	public static void main(String[] args) {
		SpringApplication.run(NpwhMemberApplication.class, args);
	}

}
