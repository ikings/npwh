package com.npwh.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.npwh.common.utils.PageUtils;
import com.npwh.member.entity.CnCityEntity;

import java.util.List;
import java.util.Map;

/**
 * 地级（城市）
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 16:55:58
 */
public interface CnCityService extends IService<CnCityEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CnCityEntity> selectByProvinceCode(String code);
}

