package com.npwh.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.npwh.common.utils.PageUtils;
import com.npwh.member.entity.AttendanceEntity;
import com.npwh.member.vo.AtterdanceVo;

import java.util.Map;

/**
 * 会员考勤记录表
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-10 10:29:37
 */
public interface AttendanceService extends IService<AttendanceEntity> {

    PageUtils queryPage(Map<String, Object> params);

    AtterdanceVo getMemberByNumber(String cardNumber);
}

