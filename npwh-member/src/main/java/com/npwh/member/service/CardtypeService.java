package com.npwh.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.npwh.common.utils.PageUtils;
import com.npwh.member.entity.CardtypeEntity;

import java.util.Map;

/**
 * 会员卡类型表 
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
public interface CardtypeService extends IService<CardtypeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

