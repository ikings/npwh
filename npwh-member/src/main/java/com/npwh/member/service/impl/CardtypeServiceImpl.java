package com.npwh.member.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.Query;

import com.npwh.member.dao.CardtypeDao;
import com.npwh.member.entity.CardtypeEntity;
import com.npwh.member.service.CardtypeService;


@Service("cardtypeService")
public class CardtypeServiceImpl extends ServiceImpl<CardtypeDao, CardtypeEntity> implements CardtypeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CardtypeEntity> page = this.page(
                new Query<CardtypeEntity>().getPage(params),
                new QueryWrapper<CardtypeEntity>()
        );

        return new PageUtils(page);
    }

}