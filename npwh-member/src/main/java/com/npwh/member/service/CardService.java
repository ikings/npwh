package com.npwh.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.npwh.common.utils.PageUtils;
import com.npwh.member.entity.CardEntity;
import com.npwh.member.vo.CardVo;

import java.util.List;
import java.util.Map;

/**
 * 会员卡表
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
public interface CardService extends IService<CardEntity> {

    PageUtils queryPage(Map<String, Object> params);

    CardVo selectByCardNumber(String cardNumber);

    Map<String, Object> cardTotalMap();
}

