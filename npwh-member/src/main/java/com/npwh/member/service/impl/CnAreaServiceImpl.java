package com.npwh.member.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.Query;

import com.npwh.member.dao.CnAreaDao;
import com.npwh.member.entity.CnAreaEntity;
import com.npwh.member.service.CnAreaService;


@Service("cnAreaService")
public class CnAreaServiceImpl extends ServiceImpl<CnAreaDao, CnAreaEntity> implements CnAreaService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CnAreaEntity> page = this.page(
                new Query<CnAreaEntity>().getPage(params),
                new QueryWrapper<CnAreaEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CnAreaEntity> selectByCityCode(String code) {
        return this.baseMapper.selectList(new QueryWrapper<CnAreaEntity>().eq("city_code", code));
    }

}