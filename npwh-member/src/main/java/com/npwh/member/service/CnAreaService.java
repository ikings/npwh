package com.npwh.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.npwh.common.utils.PageUtils;
import com.npwh.member.entity.CnAreaEntity;

import java.util.List;
import java.util.Map;

/**
 *  县级（区县）
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 16:55:57
 */
public interface CnAreaService extends IService<CnAreaEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CnAreaEntity> selectByCityCode(String code);
}

