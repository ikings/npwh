package com.npwh.member.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.Query;

import com.npwh.member.dao.CnCityDao;
import com.npwh.member.entity.CnCityEntity;
import com.npwh.member.service.CnCityService;


@Service("cnCityService")
public class CnCityServiceImpl extends ServiceImpl<CnCityDao, CnCityEntity> implements CnCityService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CnCityEntity> page = this.page(
                new Query<CnCityEntity>().getPage(params),
                new QueryWrapper<CnCityEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CnCityEntity> selectByProvinceCode(String code) {
        return this.baseMapper.selectList(new QueryWrapper<CnCityEntity>().eq("province_code", code));
    }

}