package com.npwh.member.service.impl;

import com.npwh.common.utils.DateUtils;
import com.npwh.member.dao.CardtypeDao;
import com.npwh.member.entity.CardtypeEntity;
import com.npwh.member.vo.CardTotalVo;
import com.npwh.member.vo.CardVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.Query;

import com.npwh.member.dao.CardDao;
import com.npwh.member.entity.CardEntity;
import com.npwh.member.service.CardService;
import org.springframework.transaction.annotation.Transactional;


@Service("cardService")
public class CardServiceImpl extends ServiceImpl<CardDao, CardEntity> implements CardService {

    @Autowired
    CardDao cardDao;

    @Autowired
    CardtypeDao cardtypeDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CardEntity> page = this.page(
                new Query<CardEntity>().getPage(params),
                new QueryWrapper<CardEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public CardVo selectByCardNumber(String cardNumber) {
        return baseMapper.selectByCardNumber(cardNumber);
    }

    @Override
    public Map<String, Object> cardTotalMap() {
        // 所有的卡类型
        List<CardtypeEntity> cardtypeList = cardtypeDao.selectList(new QueryWrapper<CardtypeEntity>());
        // 最近七天日期
        List<String> daysList = DateUtils.getDaysBetwwen(6);
        // 返回结果
        Map<String, Object> resultMap = new HashMap<>();
        // 近7天日期
        resultMap.put("date", daysList);

        List<Map<String, Object>> arrayList = new ArrayList<>();
        ArrayList<String> typeList = new ArrayList<>();

        cardtypeList.forEach(item -> {
            typeList.add(item.getName());

            Map<String, Object> hashMap = new HashMap<>();
            hashMap.put("name", item.getName());
            hashMap.put("type", "line");
            hashMap.put("stack", "总量");

            ArrayList<Integer> intList = new ArrayList<>();
            List<CardTotalVo> totalMap = cardDao.cardTotalMap(item.getId());
            totalMap.forEach(m -> {
                intList.add(m.getTotal());
            });
            hashMap.put("data", intList);

            arrayList.add(hashMap);
        });
        // 卡类型
        resultMap.put("type", typeList);
        // 统计
        resultMap.put("total", arrayList);

        // 今天办卡量统计
        Integer today = this.baseMapper.selectCount(
                new QueryWrapper<CardEntity>()
                        .eq("TO_DAYS(FROM_UNIXTIME(created_time))", "TO_DAYS(NOW())")
        );

        // 昨日办卡量
        Integer yesterday = this.baseMapper.selectCount(
                new QueryWrapper<CardEntity>()
                        .eq("TO_DAYS(NOW())-TO_DAYS(FROM_UNIXTIME(created_time))", "1")
        );

        // 总量
        Integer total = this.baseMapper.selectCount(new QueryWrapper<>());
        resultMap.put("today", today);
        resultMap.put("yesterday", yesterday);
        resultMap.put("totalCount", total);

        return resultMap;
    }

}