package com.npwh.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.npwh.common.utils.PageUtils;
import com.npwh.member.entity.HandleMethodEntity;

import java.util.Map;

/**
 * 办理方式表 
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
public interface HandleMethodService extends IService<HandleMethodEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

