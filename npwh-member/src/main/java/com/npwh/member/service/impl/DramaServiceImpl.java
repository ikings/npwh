package com.npwh.member.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.Query;

import com.npwh.member.dao.DramaDao;
import com.npwh.member.entity.DramaEntity;
import com.npwh.member.service.DramaService;


@Service("dramaService")
public class DramaServiceImpl extends ServiceImpl<DramaDao, DramaEntity> implements DramaService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        String status = params.get("status") + "";

        IPage<DramaEntity> page = this.page(
                new Query<DramaEntity>().getPage(params),
                new QueryWrapper<DramaEntity>()
                        .eq(StringUtils.isNotBlank(status), "status", 1)
                        .orderByAsc("view_date")
        );

        return new PageUtils(page);
    }

}