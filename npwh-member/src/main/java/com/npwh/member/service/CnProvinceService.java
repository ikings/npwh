package com.npwh.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.npwh.common.utils.PageUtils;
import com.npwh.member.entity.CnProvinceEntity;

import java.util.Map;

/**
 * 省级（省份直辖市自治区）
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 16:55:58
 */
public interface CnProvinceService extends IService<CnProvinceEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

