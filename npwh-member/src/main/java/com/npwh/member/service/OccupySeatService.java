package com.npwh.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.npwh.common.utils.PageUtils;
import com.npwh.member.entity.OccupySeatEntity;

import java.util.Map;

/**
 * 剧场座位号发放
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-30 22:31:07
 */
public interface OccupySeatService extends IService<OccupySeatEntity> {

    PageUtils queryPage(Map<String, Object> params);

    int getSeatNo(String dramaId, String cardNumber);
}

