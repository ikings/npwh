package com.npwh.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.npwh.common.utils.PageUtils;
import com.npwh.member.entity.CardRenewEntity;

import java.util.Map;

/**
 * 会员续费日志记录表
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-09 10:52:32
 */
public interface CardRenewService extends IService<CardRenewEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

