package com.npwh.member.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.npwh.member.entity.CardEntity;
import com.npwh.member.entity.CardtypeEntity;
import com.npwh.member.entity.OccupySeatEntity;
import com.npwh.member.service.CardService;
import com.npwh.member.service.CardtypeService;
import com.npwh.member.service.OccupySeatService;
import com.npwh.member.vo.AtterdanceVo;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.npwh.member.entity.AttendanceEntity;
import com.npwh.member.service.AttendanceService;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.R;


/**
 * 会员考勤记录表
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-10 10:29:37
 */
@RestController
@RequestMapping("member/attendance")
public class AttendanceController {

    private AttendanceService attendanceService;

    @Autowired
    public void setAttendanceService(AttendanceService attendanceService) {
        this.attendanceService = attendanceService;
    }

    private CardService cardService;

    @Autowired
    public void setCardService(CardService cardService) {
        this.cardService = cardService;
    }

    private CardtypeService cardtypeService;

    @Autowired
    public void setCardtypeService(CardtypeService cardtypeService) {
        this.cardtypeService = cardtypeService;
    }

    private OccupySeatService occupySeatService;

    @Autowired
    public void setOccupySeatService(OccupySeatService occupySeatService) {
        this.occupySeatService = occupySeatService;
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("member:attendance:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = attendanceService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("member:attendance:info")
    public R info(@PathVariable("id") Integer id) {
        AttendanceEntity attendance = attendanceService.getById(id);

        return R.ok().put("attendance", attendance);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("member:attendance:save")
    public R save(@RequestBody AttendanceEntity attendance) {
        // 查询剩余可用次数
        CardEntity cardEntity = cardService.getById(attendance.getCardId());
        if (cardEntity == null) {
            return R.error("会员卡不存在");
        }
        if (cardEntity.getUsage() <= 0) {
            return R.error("打卡可用次数为0,请充值");
        }

        // 判断会员卡是否在有效期内
        long expDate = cardEntity.getExpDate();
        long currDate = new Date().getTime() / 1000;

        if (currDate > expDate) {
            return R.error("会员卡已过期");
        }

        // 查询会员卡类型
        CardtypeEntity cardtypeEntity = cardtypeService.getById(attendance.getCardTypeId());

        // TODO 限制刷卡次数
        // 如果限制次数大于0，则进行限制，等于0 则是无限制刷卡
        if (cardtypeEntity.getConstrains() > 0) {
            // 当前日期
            String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            // 当日只能打卡一次, 如果存在打卡记录
            // AttendanceEntity one = attendanceService.lambdaQuery()
            //         .eq(AttendanceEntity::getCardId, attendance.getCardId())
            //         .orderByDesc(AttendanceEntity::getPunchTime).last("limit 1").one();
            List<AttendanceEntity> list = attendanceService.list(
                    new QueryWrapper<AttendanceEntity>()
                            .eq("card_id", attendance.getCardId())
                            .eq("FROM_UNIXTIME(punch_time, '%Y-%m-%d')", currentDate)
                            .orderByDesc("punch_time")
            );
            if (list.size() >= cardtypeEntity.getConstrains()) {
                return R.error("今天已超过打卡限制了");
            }
            // if (one != null) {
            //     // AttendanceEntity one = entityList.get(0);
            //     // 上次打卡时间
            //     Long punchTime = one.getPunchTime();
            //     String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date(punchTime * 1000));
            //
            //
            //     if (format.equals(currentDate)) {
            //         return R.error("今天已过打卡了");
            //     }
            // }
        }
        // TODO END

        // 打卡时间
        attendance.setPunchTime(new Date().getTime() / 1000);
        // 记录的创建时间
        attendance.setCreatedTime(new Date().getTime() / 1000);
        // 新增打卡记录
        attendanceService.save(attendance);

        // 从总的可用次数上减去1
        CardEntity entity = new CardEntity();
        entity.setId(cardEntity.getId());
        entity.setUsage(cardEntity.getUsage() - 1);
        cardService.updateById(entity);

        // 记录座位号
        if (StringUtils.isNotBlank(attendance.getSeat()) && StringUtils.isNotBlank(attendance.getDramaId())) {
            OccupySeatEntity seatEntity = new OccupySeatEntity();
            seatEntity.setCardId(attendance.getCardId());
            seatEntity.setMemberId(attendance.getMemberId());
            seatEntity.setDramaId(Integer.parseInt(attendance.getDramaId()));
            seatEntity.setSeatNumber(Integer.parseInt(attendance.getSeat()));
            seatEntity.setCreatedTime(new Date().getTime() / 1000);
            occupySeatService.save(seatEntity);
        }

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("member:attendance:update")
    public R update(@RequestBody AttendanceEntity attendance) {
        attendanceService.updateById(attendance);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete/{cardNumber}")
    // @RequiresPermissions("member:attendance:delete")
    public R delete(@RequestBody Integer[] ids, @PathVariable("cardNumber") String cardNumber) {
        boolean b = attendanceService.removeByIds(Arrays.asList(ids));
        // 删除成功后，可用次数+1
        if (b) {
            // 查询剩余可用次数
            CardEntity cardEntity = cardService.getOne(new QueryWrapper<CardEntity>().eq("card_number", cardNumber));
            // 从总的可用次数加上1
            CardEntity entity = new CardEntity();
            entity.setId(cardEntity.getId());
            entity.setUsage(cardEntity.getUsage() + 1);
            cardService.updateById(entity);
        }
        return R.ok();
    }

    /**
     * 查询会员信息
     *
     * @param cardNumber 会员卡号
     * @return AtterdanceVo
     */
    @RequestMapping("/selectMember/{cardNumber}")
    public R selectMember(@PathVariable String cardNumber) {
        AtterdanceVo vo = attendanceService.getMemberByNumber(cardNumber);
        return R.ok().put("data", vo);
    }

    @RequestMapping("/allow")
    public R allowAttendance(@RequestBody Map<String, Object> params) {
        String cardId = params.get("cardId") + "";
        String cardTypeId = params.get("cardTypeId") + "";
        String dramaId = params.get("dramaId") + "";

        // 查询剩余可用次数
        CardEntity cardEntity = cardService.getById(cardId);
        if (cardEntity == null) {
            return R.error("会员卡不存在");
        }
        if (cardEntity.getUsage() <= 0) {
            return R.error("打卡可用次数为0,请充值");
        }

        // 查询会员卡类型
        CardtypeEntity cardtypeEntity = cardtypeService.getById(cardTypeId);

        // TODO 限制刷卡次数
        // 如果限制次数大于0，则进行限制，等于0 则是无限制刷卡
        if (cardtypeEntity.getConstrains() > 0) {
            // 当前日期
            String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            List<AttendanceEntity> list = attendanceService.list(
                    new QueryWrapper<AttendanceEntity>()
                            .eq("card_id", cardId)
                            .eq("FROM_UNIXTIME(punch_time, '%Y-%m-%d')", currentDate)
                            .orderByDesc("punch_time")
            );
            if (list.size() >= cardtypeEntity.getConstrains()) {
                return R.error("今天已超过打卡限制了");
            }
        }
        int seatNo = occupySeatService.getSeatNo(dramaId, cardEntity.getCardNumber());
        // TODO END
        return R.ok().put("data", seatNo);
    }

}
