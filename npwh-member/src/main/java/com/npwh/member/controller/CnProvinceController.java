package com.npwh.member.controller;

import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.npwh.member.entity.CnProvinceEntity;
import com.npwh.member.service.CnProvinceService;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.R;


/**
 * 省级（省份直辖市自治区）
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 16:55:58
 */
@RestController
@RequestMapping("member/cnprovince")
public class CnProvinceController {
    @Autowired
    private CnProvinceService cnProvinceService;

    @RequestMapping("/districts")
    public R selectList() {
        try {
            ClassPathResource pathResource = new ClassPathResource("/json/pca-code.json");
            String str = IOUtils.toString(new InputStreamReader(pathResource.getInputStream(), "UTF-8"));
            return R.ok().put("data", JSONObject.parseObject(str, HashMap.class));
        } catch (Exception e) {
            return R.error(e.getMessage());
        }
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("member:cnprovince:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = cnProvinceService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{prid}")
    // @RequiresPermissions("member:cnprovince:info")
    public R info(@PathVariable("prid") Integer prid) {
        CnProvinceEntity cnProvince = cnProvinceService.getById(prid);

        return R.ok().put("cnProvince", cnProvince);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("member:cnprovince:save")
    public R save(@RequestBody CnProvinceEntity cnProvince) {
        cnProvinceService.save(cnProvince);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("member:cnprovince:update")
    public R update(@RequestBody CnProvinceEntity cnProvince) {
        cnProvinceService.updateById(cnProvince);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("member:cnprovince:delete")
    public R delete(@RequestBody Integer[] prids) {
        cnProvinceService.removeByIds(Arrays.asList(prids));

        return R.ok();
    }

}
