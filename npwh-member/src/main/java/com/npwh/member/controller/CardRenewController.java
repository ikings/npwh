package com.npwh.member.controller;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.npwh.member.entity.CardEntity;
import com.npwh.member.service.CardService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.npwh.member.entity.CardRenewEntity;
import com.npwh.member.service.CardRenewService;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.R;


/**
 * 会员续费日志记录表
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-09 10:52:32
 */
@RestController
@RequestMapping("member/cardrenew")
public class CardRenewController {

    private CardRenewService cardRenewService;

    @Autowired
    public void setCardRenewService(CardRenewService cardRenewService) {
        this.cardRenewService = cardRenewService;
    }

    private CardService cardService;

    @Autowired
    public void setCardService(CardService cardService) {
        this.cardService = cardService;
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("member:cardrenew:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = cardRenewService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("member:cardrenew:info")
    public R info(@PathVariable("id") Integer id) {
        CardRenewEntity cardRenew = cardRenewService.getById(id);

        return R.ok().put("cardRenew", cardRenew);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("member:cardrenew:save")
    public R save(@RequestBody CardRenewEntity cardRenew) {

        cardRenew.setCreatedTime(new Date().getTime() / 1000);

        cardRenewService.save(cardRenew);

        return R.ok();
    }

    @RequestMapping("/v2/save")
    public R save_v2(@RequestBody Map<String, Object> params) {

        CardRenewEntity renewEntity = new CardRenewEntity();
        Integer cardId;
        // 会员编号
        if (StringUtils.isNotBlank(params.get("cardId") + "")) {
            cardId = Integer.parseInt(params.get("cardId") + "");
            renewEntity.setCardId(cardId);
        } else {
            return R.error("会员卡编号不能为空");
        }

        // 查询会员卡数据
        CardEntity entity = cardService.getById(cardId);

        // 充值金额
        if (StringUtils.isNotBlank(params.get("money") + "")) {
            renewEntity.setMoney(BigDecimal.valueOf(Long.parseLong(params.get("money") + "")));
        }

        Integer usage;
        // 可用次数
        if (StringUtils.isNotBlank(params.get("usage") + "")) {
            // 当前充值可用次数
            usage = Integer.parseInt(params.get("usage") + "");
            renewEntity.setUsage(usage);
            // 上次剩余可用次数
            renewEntity.setLastusage(entity.getUsage());
            // 当前总的可用次数
            renewEntity.setTotalusage(usage + entity.getUsage());
        } else {
            return R.error("可用次数不能为空");
        }

        // 有效日期
        if (StringUtils.isNotBlank(params.get("expiryDate") + "")) {
            renewEntity.setExpiryDate(Long.parseLong(params.get("expiryDate") + ""));
        }

        // 备注
        if (StringUtils.isNotBlank(params.get("remark") + "")) {
            renewEntity.setRemark(params.get("remark") + "");
        }

        // 操作人
        if (StringUtils.isNotBlank(params.get("createdBy") + "")) {
            renewEntity.setCreatedBy(params.get("createdBy") + "");
        }

        renewEntity.setCreatedTime(new Date().getTime() / 1000);

        // 保存续费日志记录
        cardRenewService.save(renewEntity);

        // 组装更新后的可用次数进行数据记录更新
        CardEntity cardEntity = new CardEntity();
        if (renewEntity.getMoney().doubleValue() > 0) {
            // 卡内剩余余额加上当前续期充值的金额=当前总的余额
            cardEntity.setBalance(entity.getBalance().add(renewEntity.getMoney()));
        }

        cardEntity.setId(Integer.parseInt(params.get("cardId") + ""));
        cardEntity.setUsage(usage + entity.getUsage());

        // 更新可用次数
        cardService.updateById(cardEntity);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("member:cardrenew:update")
    public R update(@RequestBody CardRenewEntity cardRenew) {
        cardRenewService.updateById(cardRenew);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("member:cardrenew:delete")
    public R delete(@RequestBody Integer[] ids) {
        cardRenewService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
