package com.npwh.member.controller;

import java.util.Arrays;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.npwh.member.entity.DictionaryEntity;
import com.npwh.member.service.DictionaryService;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.R;



/**
 * 数据字典表 
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@RestController
@RequestMapping("member/dictionary")
public class DictionaryController {
    @Autowired
    private DictionaryService dictionaryService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("member:dictionary:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dictionaryService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("member:dictionary:info")
    public R info(@PathVariable("id") Integer id){
		DictionaryEntity dictionary = dictionaryService.getById(id);

        return R.ok().put("dictionary", dictionary);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("member:dictionary:save")
    public R save(@RequestBody DictionaryEntity dictionary){
		dictionaryService.save(dictionary);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("member:dictionary:update")
    public R update(@RequestBody DictionaryEntity dictionary){
		dictionaryService.updateById(dictionary);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("member:dictionary:delete")
    public R delete(@RequestBody Integer[] ids){
		dictionaryService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
