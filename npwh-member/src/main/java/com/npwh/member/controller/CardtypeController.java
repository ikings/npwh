package com.npwh.member.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.npwh.member.entity.CardtypeEntity;
import com.npwh.member.service.CardtypeService;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.R;



/**
 * 会员卡类型表 
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@RestController
@RequestMapping("member/cardtype")
public class CardtypeController {
    @Autowired
    private CardtypeService cardtypeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("member:cardtype:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = cardtypeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("member:cardtype:info")
    public R info(@PathVariable("id") Integer id){
		CardtypeEntity cardtype = cardtypeService.getById(id);

        return R.ok().put("cardtype", cardtype);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("member:cardtype:save")
    public R save(@RequestBody CardtypeEntity cardtype){

        cardtype.setCreatedBy("kings");
        cardtype.setCreatedTime(new Date().getTime() / 1000);

		cardtypeService.save(cardtype);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("member:cardtype:update")
    public R update(@RequestBody CardtypeEntity cardtype){
		cardtypeService.updateById(cardtype);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("member:cardtype:delete")
    public R delete(@RequestBody Integer[] ids){
		cardtypeService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
