package com.npwh.member.controller;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.npwh.member.entity.DramaEntity;
import com.npwh.member.service.DramaService;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.R;


/**
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-28 14:39:09
 */
@RestController
@RequestMapping("member/drama")
public class DramaController {
    @Autowired
    private DramaService dramaService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("member:drama:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = dramaService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("member:drama:info")
    public R info(@PathVariable("id") Integer id) {
        DramaEntity drama = dramaService.getById(id);

        return R.ok().put("drama", drama);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("member:drama:save")
    public R save(@RequestBody DramaEntity drama) {

        // 记录创建时间，时间戳
        drama.setCreatedTime(Instant.now().getEpochSecond());

        dramaService.save(drama);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("member:drama:update")
    public R update(@RequestBody DramaEntity drama) {

        // 记录更新时间，时间戳
        drama.setUpdatedTime(Instant.now().getEpochSecond());

        dramaService.updateById(drama);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("member:drama:delete")
    public R delete(@RequestBody Integer[] ids) {
        dramaService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
