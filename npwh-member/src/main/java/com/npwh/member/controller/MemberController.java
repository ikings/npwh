package com.npwh.member.controller;

import java.util.*;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.npwh.member.entity.*;
import com.npwh.member.service.*;
import com.npwh.member.vo.MemberVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.R;


/**
 * 会员基础资料表
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@RestController
@RequestMapping("member/member")
public class MemberController {
    /**
     * 1. controller 处理请求，接收和校验数据
     * 2. service 接收controller传来的数据，进行业务处理
     * 3. controller接收service处理完的数据，封装页面指定结果数据
     */
    @Autowired
    private MemberService memberService;

    @Autowired
    private CardService cardService;

    @Autowired
    private CardRenewService cardRenewService;

    @Autowired
    private AttendanceService attendanceService;

    @Autowired
    private CnProvinceService provinceService;

    @Autowired
    private CnCityService cityService;

    @Autowired
    private CnAreaService areaService;

    @Value("${upload.access.path}")
    private String domainName;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("member:member:list")
    public R list(@RequestParam Map<String, Object> params) {
        try {
            // PageUtils page = memberService.queryPage(params);

            Page<MemberVo> page = memberService.queryPageList(params);

            return R.ok().put("page", page);
        } catch (Exception e) {
            return R.error(e.getMessage());
        }
    }

    /**
     * 获取会员详细信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("member:member:info")
    public R info(@PathVariable("id") Integer id) {
        MemberEntity member = memberService.getById(id);

        if (member != null && StringUtils.isNotBlank(member.getAvatar()) ) {
            // 头像
            member.setAvatar(domainName + member.getAvatar());
        }

        return R.ok().put("member", member);
    }

    /**
     * 按指定列作为条件查询数据
     * @param column
     * @param val
     * @return
     */
    @RequestMapping("/select/{column}/{val}")
    public R select(@PathVariable("column") String column,
                    @PathVariable("val") String val) {
        List<MemberEntity> list = memberService.list(new QueryWrapper<MemberEntity>().like(column, val));
        list.forEach(item -> item.setName(item.getName() + " (" + item.getTelphone() + ")"));
        return R.ok().put("data", list);
    }

    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") String id) {

        Map<String, Object> resultMap = new HashMap<>();

        // 获取会员基本信息
        MemberEntity memberEntity = memberService.getById(id);
        if (memberEntity == null) {
            return R.error("会员信息不存在");
        }
        memberEntity.setAvatar(domainName + memberEntity.getAvatar());
        resultMap.put("member", memberEntity);

        // 按会员ID查询会员卡信息
        List<CardEntity> cardList = cardService.lambdaQuery().eq(CardEntity::getMemberId, memberEntity.getId()).list();
        resultMap.put("card", cardList);

        // 按会员卡ID查询充值数据
        // Map<String, Object> renewMap = new HashMap<>();
        for (CardEntity entity : cardList) {
            List<CardRenewEntity> list = cardRenewService.lambdaQuery().eq(CardRenewEntity::getCardId, entity.getId()).list();
            resultMap.put("renew", list);
        }

        // 按会员资料编号查询打卡记录
        List<AttendanceEntity> attenList = attendanceService.lambdaQuery().eq(AttendanceEntity::getMemberId, memberEntity.getId()).list();
        resultMap.put("attendance", attenList);

        // 按会员资料编号查询推荐记录
        List<CardEntity> referrerList = cardService.lambdaQuery().eq(CardEntity::getReferrerId, memberEntity.getId()).list();
        referrerList.forEach(item -> {
            item.setMember(memberService.getById(item.getMemberId()));
        });
        resultMap.put("referrer", referrerList);

        return R.ok().put("data", resultMap);
    }

    @RequestMapping("/select/{name}")
    public R selectByName(@PathVariable("name") String name) {
        List<MemberEntity> list = memberService.selectByName(name);

        return R.ok().put("data", list);
    }

    @RequestMapping("/select/idcard/{idcard}")
    public R selectByIdCard(@PathVariable("idcard") String idcard) {
        List<MemberEntity> memberEntity = memberService.selectByIdCard(idcard);

        return R.ok().put("data", memberEntity);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("member:member:save")
    public R save(@RequestBody MemberEntity member) {

        // 查询省份并赋值
        if (StringUtils.isNotBlank(member.getProvincecode())) {
            CnProvinceEntity province = provinceService.getOne(
                    new QueryWrapper<CnProvinceEntity>()
                            .eq("code", member.getProvincecode())
            );
            if (province != null) {
                member.setProvincename(province.getName());
            }
        }
        // 查询城市并赋值
        if (StringUtils.isNotBlank(member.getCitycode())) {
            CnCityEntity city = cityService.getOne(
                    new QueryWrapper<CnCityEntity>()
                            .eq("code", member.getCitycode())
            );
            if (city != null) {
                member.setCityname(city.getName());
            }
        }
        // 查询地区并赋值
        if (StringUtils.isNotBlank(member.getAreacode())) {
            CnAreaEntity area = areaService.getOne(
                    new QueryWrapper<CnAreaEntity>()
                            .eq("code", member.getAreacode())
            );
            if (area != null) {
                member.setAreaname(area.getName());
            }
        }

        member.setCreatedTime(new Date().getTime() / 1000);
        member.setStatus(1);

        memberService.save(member);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("member:member:update")
    public R update(@RequestBody MemberEntity member) {

        // 查询省份并赋值
        if (StringUtils.isNotBlank(member.getProvincecode())) {
            CnProvinceEntity province = provinceService.getOne(
                    new QueryWrapper<CnProvinceEntity>()
                            .eq("code", member.getProvincecode())
            );
            if (province != null) {
                member.setProvincename(province.getName());
            }
        }
        // 查询城市并赋值
        if (StringUtils.isNotBlank(member.getCitycode())) {
            CnCityEntity city = cityService.getOne(
                    new QueryWrapper<CnCityEntity>()
                            .eq("code", member.getCitycode())
            );
            if (city != null) {
                member.setCityname(city.getName());
            }
        }
        // 查询地区并赋值
        if (StringUtils.isNotBlank(member.getAreacode())) {
            CnAreaEntity area = areaService.getOne(
                    new QueryWrapper<CnAreaEntity>()
                            .eq("code", member.getAreacode())
            );
            if (area != null) {
                member.setAreaname(area.getName());
            }
        }

        // 数据更新时间
        member.setUpdatedTime(new Date().getTime() / 1000);

        memberService.updateById(member);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("member:member:delete")
    public R delete(@RequestBody Integer id) {
        // memberService.removeByIds(Arrays.asList(ids));

        MemberEntity entity = new MemberEntity();
        entity.setStatus(0);
        entity.setId(id);
        memberService.updateById(entity);

        return R.ok();
    }

}
