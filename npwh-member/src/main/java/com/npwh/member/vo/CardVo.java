package com.npwh.member.vo;

import com.npwh.member.entity.DramaEntity;
import lombok.Data;

import java.util.List;

@Data
public class CardVo {
    private Integer id;         // 会员卡ID
    private Integer memberId;   // 会员资料编号
    private String name;        // 姓名
    private String telphone;    // 电话
    private String idcard;      // 身份证号
    private Integer cardTypeId; // 会员卡类型ID
    private String cardTypeName;    // 会员卡类型
    private String method;      // 会员卡办理方式
    private Integer usage;      // 剩余可用次数
    private String seat;        // 座位号
    private List<DramaEntity> dramas;   // 戏曲数组
}
