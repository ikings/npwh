package com.npwh.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

// exclude = {DataSourceAutoConfiguration.class 网关不需要数据库配置相关的东西，排除掉数据源相关的配置
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class NpwhGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(NpwhGatewayApplication.class, args);
	}

}
